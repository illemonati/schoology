package schoology

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

//GetOAuthHeader gets the schoology
//oauth header, oauthToken and oauthTokenSecret are optional, use empty string if you don't have one
func GetOAuthHeader(consumerKey, consumerSecret, oauthToken, oauthTokenSecret string) http.Header {
	var header http.Header
	header.Add("OAuth realm", "Schoology API")
	header.Add("oauth_consumer_key", consumerKey)
	header.Add("oauth_token", oauthToken)
	header.Add("oauth_nonce", generateNonce())
	header.Add("oauth_timestamp", time.Now().String())
	header.Add("oauth_signature_method", "PLAINTEXT")
	header.Add("oauth_version", "1.0")
	header.Add("oauth_signature", fmt.Sprintf("%s%%26%s", consumerSecret, oauthTokenSecret))
	return header
}

func generateNonce() string {
	rand.Seed(time.Now().UnixNano())
	nonce := rand.Intn(100000000)
	noncestr := strconv.Itoa(nonce)
	return noncestr
}
