#[macro_use]
extern crate serde_derive;
mod auth;
mod methods;

use self::auth::Auth;
use self::methods::get_flex_updates;

fn main(){
    let oauth = Auth::new("fd48cd30416207f1631274ebf6cbce1105b6f54c5", "53dc2ac9db02e81d846cda49647e8918");
    let headers = oauth.request_headers();
    println!("{:#?}", headers);
    println!("{:#?}",get_flex_updates(headers));

}
