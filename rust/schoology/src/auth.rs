extern crate rand;
// extern crate hyper;
extern crate reqwest;
extern crate hyper;
use crate::auth::rand::Rng;
use std::time;
// use std::collections::HashMap;

use hyper::header::HeaderMap;
use hyper::header::*;
// use reqwest::header::Authorization;
// use reqwest::header::Accept;

// header! { (Authorization, "Authorization") => [String] }
// header! { (Accept, "Accept") => [String] }
// header! { (Host, "Host") => [String] }
// header! { (ContentType, "Content-Type") => [String] }

pub struct Auth{
    consumer_key: &'static str,
    consumer_secret: &'static str,
}

impl Auth{
    pub fn new(consumer_key: &'static str, consumer_secret: &'static str) -> Auth{
        Auth{consumer_key: consumer_key, consumer_secret: consumer_secret}
    }
    pub fn oauth_headers(&self) -> String{
        let nonce: String = rand::thread_rng()
            .gen_ascii_chars()
            .take(8)
            .collect::<String>();
        let auth: String = format!("OAuth realm=\"Schoology API\",oauth_consumer_key=\"{}\",oauth_token=\"\",oauth_nonce=\"{}\",oauth_timestamp=\"{:#?}\",oauth_signature_method=\"PLAINTEXT\",oauth_version=\"1.0\", oauth_signature=\"{}\"", self.consumer_key, nonce, time::SystemTime::now().duration_since(time::SystemTime::UNIX_EPOCH).unwrap().as_secs(), self.consumer_secret);
        println!("{}",auth);
        return auth;
    }
    pub fn request_headers(&self) -> HeaderMap{
        // let headers : String = format!("\"Authorization\": {}, \"Accept\": \"application/json\", \"Host\": \"api.schoology.com\", \"Content-Type\": \"application/json\"", self.oauth_headers());
        let mut headers = HeaderMap::new();
        // headers.set(Authorization(self.oauth_header().to_owned()));
        // headers.set(Accept("application/json".to_owned()));
        // headers.set(Host("api.schoology.com".to_owned()));
        // headers.set(ContentType("application/json".to_owned()));
        let mut headers = HeaderMap::new();
        headers.insert(AUTHORIZATION, self.oauth_headers().parse().unwrap());
        headers.insert(ACCEPT, "application/json".parse().unwrap());
        headers.insert(HOST, "api.schoology.com".parse().unwrap());
        headers.insert(CONTENT_TYPE, "application/json".parse().unwrap());
        return headers;
    }
}
