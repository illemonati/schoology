extern crate serde;
extern crate serde_json;
extern crate reqwest;
extern crate hyper;
use hyper::header::HeaderMap;

const base_endpoint: &'static str = "https://api.schoology.com/v1/";
// const base_endpoint: &'static str = "https://webhook.site/9d4928cb-685b-49b0-bc80-52a5c74fe1e4/";

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Links {
  #[serde(rename = "self")]
  _self: String,
  next: String,
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Updates {
  update: Vec<Update>,
  links: Links,
}

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct Update {
  id: i64,
  body: String,
  uid: i64,
  created: i64,
  likes: i64,
  user_like_action: bool,
  realm: String,
  section_id: i64,
  num_comments: i64,
}

pub fn get_flex_updates(headers: HeaderMap) -> Result<Updates, reqwest::Error> {
    let endpoint: String = format!("{}course/1887085609/updates", base_endpoint);
    let updates: Updates = reqwest::Client::new().get(endpoint.as_str()).headers(headers).send()?.json()?;
    return Ok(updates);
}
